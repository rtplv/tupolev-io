const express = require('express');
const router = express.Router();

const HubController = require('../controllers/HubController');

router.get('/hub/links', HubController.getAll);
router.get('/hub/link/:id', HubController.getLinkById);

router.post('/hub/link/create', HubController.createLink);
router.put('/hub/link/update', HubController.updateLink);
router.delete('/hub/link/delete', HubController.deleteLink);

module.exports = router;
