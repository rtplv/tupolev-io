// Base libraries
const fs = require('fs');
const path = require('path');

// Express and plugins
const express = require('express');
const compression = require('compression');
const cors = require('cors');
const morgan = require('morgan');
const rfs = require('rotating-file-stream');
const { generator } = require('./utils/logs');
const helmet = require('helmet');

/* Add logging */
const logDirectory = path.join(__dirname, 'logs');
// Create directory if it not exists
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);
// Create rotate file stream
const accessLogStream = rfs(generator, {
  size: '10M',
  interval: '1d', // rotate daily
  path: logDirectory,
});

/* Init app */
const server = express();

// Add assets
server.use(morgan('combined', { stream: accessLogStream }));
server.use(express.json()); // replaced a body-parser
server.use(compression());
server.use(cors());
server.use(helmet());

// Routes
require('./routes').applyTo(server);

module.exports = server;
