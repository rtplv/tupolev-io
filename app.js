// env variables add
require('dotenv').config();
// App constant from .env
const { DB_URL } = process.env;
const { HTTP_PORT, HTTPS_PORT } = require('./config/server.config');
const dev = process.env.NODE_ENV !== 'production';

const https = require('https');
const mongoose = require('./api/node_modules/mongoose');
const apiServer = require('./api/server');
const getSSLCerts = require('./ssl_certs');

const next = require('next');
const conf = require('./next.config');
const app = next({ dev, conf });
const handle = app.getRequestHandler();

(async () => {
  try {
    await app.prepare();

    apiServer.get('*', (req, res) => {
      return handle(req, res);
    });

    await mongoose.connect(DB_URL);
    console.log(`💾 Connect with MongoDB on ${DB_URL} port is success!`);

    apiServer.listen(HTTP_PORT, err => {
      if (err) throw err;
      console.log(`👍 API server running on ${HTTP_PORT} port`);
    });

    // https server
    if (!dev) {
      const certs = getSSLCerts();
      https.createServer(certs, apiServer).listen(HTTPS_PORT, err => {
        if (err) throw err;
        console.log(`👍 API server with SSL running on ${HTTPS_PORT} port`);
      });
    }
  } catch (err) {
    console.error(err.stack);
    process.exit(1);
  }
})();
