import React, { Component } from 'react';
import PropTypes from 'prop-types';

class AuthPanel extends Component {
  static propTypes = {
    view: PropTypes.oneOf(['login', 'register', 'reset']),
  };

  state = {};

  render() {
    return (
      <section className="auth-panel">
        <article className="auth-panel__header">Лого</article>
        <article className="auth-panel__body">
          <form onSubmit={this.loginFormSubmitHandler}>
            <input type="text" />
          </form>
        </article>
        <article className="auth-panel__footer">Ссылки</article>
      </section>
    );
  }

  loginFormSubmitHandler = event => {
    event.stopPropagation();
  };
}

export default AuthPanel;
