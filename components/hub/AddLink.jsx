import React, { Component } from 'react';
import { Dropdown, Button, Form } from 'semantic-ui-react';

import HubService from '~/services/HubService';

const mockOptions = [
  { key: 1, text: 'React', value: 'react' },
  { key: 2, text: 'Frontend', value: 'frontend' },
  { key: 3, text: 'Backend', value: 'backend' },
];

export default class AddLink extends Component {
  state = {
    addLinkForm: {
      group: 'react',
    },
  };

  render() {
    const { addLinkForm } = this.state;
    return (
      <div className="hub-add-link">
        <Form onSubmit={this.onAddLinkFormSubmit}>
          <Form.Group widths="equal">
            <Form.Select
              name="group"
              label="Группа:"
              options={mockOptions}
              value={addLinkForm.group}
              onChange={this.onGroupSelectChangeHandler}
            />

            <Form.Field>
              <label>Ссылка:</label>
              <input
                type="text"
                name="url"
                onChange={this.onFormInputChangeHandler}
              />
            </Form.Field>

            <Form.Field>
              <label>Описание:</label>
              <input
                type="text"
                name="description"
                onChange={this.onFormInputChangeHandler}
              />
            </Form.Field>
          </Form.Group>

          <Button basic color="green" type="submit">
            Добавить
          </Button>
        </Form>
      </div>
    );
  }

  onFormInputChangeHandler = e => {
    const target = e.target;
    const name = target.name;
    const value = target.type === 'checkbox' ? target.checked : target.value;

    this.setState({
      addLinkForm: {
        ...this.state.addLinkForm,
        [name]: value,
      },
    });
  };

  onGroupSelectChangeHandler = (e, properties) => {
    const { value } = properties;
    this.setState({
      addLinkForm: {
        ...this.state.addLinkForm,
        group: value,
      },
    });
  };

  onAddLinkFormSubmit = async event => {
    const {
      addLinkForm: { group, url, description },
    } = this.state;
    event && event.preventDefault();

    if (group && url && description) {
      const res = await HubService.createLink(this.state.addLinkForm);
      console.log(res);
    }

    console.log(this.state.addLinkForm);
  };
}
