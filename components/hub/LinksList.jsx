import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Card, Icon } from 'semantic-ui-react';

import HubService from '~/services/HubService';

export default class LinksList extends Component {
  static propTypes = {
    links: PropTypes.arrayOf(
      PropTypes.shape({
        _id: PropTypes.string,
        group: PropTypes.string,
        url: PropTypes.string,
        description: PropTypes.string,
      }),
    ),
  };

  static defaultProps = {
    links: [],
  };

  render() {
    const { links } = this.props;
    return (
      <div className="hub-links-list">
        {links.map(link => (
          <Card
            href={`/hub/link/${link._id}`}
            key={link._id}
            header={link.url}
            meta={link.group}
            description={link.description}
          />
        ))}
      </div>
    );
  }

  removeLink = async id => {
    if (!id) {
      throw new Error('Id is not found');
    }

    const res = await HubService.deleteLink({ id });

    if (res.success) {
      alert('Успешно удалено');
    }
  };
}
