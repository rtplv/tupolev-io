import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './LoadMoreButton.scss';

class LoadMoreButton extends Component {
  static propTypes = {
    onClick: PropTypes.func,
  };

  state = {};

  render() {
    return (
      <button className="load-more-button" onClick={this.onClickHandler}>
        Загрузить ещё
      </button>
    );
  }

  onClickHandler = e => {
    if (this.onClick) {
      this.onClick(e);
    }
  };
}

export default LoadMoreButton;
