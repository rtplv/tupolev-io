import React, { Component } from 'react';
import Link from 'next/link';
import PropTypes from 'prop-types';

import './PostMiniature.scss';

class PostMiniature extends Component {
  static propTypes = {
    post: PropTypes.object,
  };

  state = {};

  render() {
    const { post } = this.props;
    return (
      <article className="blog-post-miniature is-loaded">
        <div className="post-image">
          <Link href={{ pathname: '/post', query: { id: post.id } }}>
            <a>
              <img src={post.image} alt="обложка поста" />
            </a>
          </Link>
        </div>
        <div className="post-info">
          <div className="post-tag">
            <Link href={{ pathname: '/tag', query: { name: post.tag } }}>
              <a>{post.tag}</a>
            </Link>
          </div>
          <div className="post-date">{post.date}</div>
          <div className="post-title">
            <Link href={{ pathname: '/post', query: { id: post.id } }}>
              <a>{post.title}</a>
            </Link>
          </div>
        </div>
      </article>
    );
  }
}

export default PostMiniature;
