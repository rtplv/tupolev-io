import React, { Component } from 'react';
import PropTypes from 'prop-types';

import PostMiniature from './PostMiniature';
import LoadMoreButton from '~/components/index/LoadMoreButton';

import './PostsContainer.scss';

class PostsContainer extends Component {
  static propTypes = {
    posts: PropTypes.array,
  };

  state = {
    posts: [],
  };

  async componentDidMount() {
    // temporary
    for (let i = 0; i < 25; i++) {
      await this.setState({
        ...this.state,
        posts: [
          ...this.state.posts,
          {
            id: ++i,
            image:
              'https://cdn-images-1.medium.com/max/800/1*9DpPAaEzB417yyGAyzawPA.jpeg',
            tag: 'Javascript',
            date: '6 сентября 2018',
            title: `10 вещей, которые вы в конечном итоге узнаете о JavaScript-проектах`,
          },
        ],
      });
    }
  }

  render() {
    const { posts } = this.state;
    return (
      <section className="blog-posts-container">
        <section className="container-articles">
          {posts.map(post => (
            <PostMiniature post={post} key={post.id} />
          ))}
        </section>
        <section className="container-footer">
          <LoadMoreButton />
        </section>
      </section>
    );
  }
}

export default PostsContainer;
