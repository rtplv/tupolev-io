import React, { Component } from 'react';
import PropTypes from 'prop-types';

import PostHeader from './PostHeader';
import PostFooter from './PostFooter';

import './Post.scss';

class Post extends Component {
  static propTypes = {
    post: PropTypes.object,
  };

  state = {
    meta: {
      title:
        '10 вещей, которые вы в конечном итоге узнаете о JavaScript-проектах',
      dateCreated: 1545091200,
      tag: 'Javascript',
    },
    tags: ['Javascript', 'Начальный уровень', 'Проекты'],
  };

  render() {
    const { meta, tags } = this.state;
    return (
      <section className="blog-post">
        <PostHeader meta={meta} />

        <hr className="blog-post__delimiter" />

        <article className="blog-post__content">
          У меня нет огромных задач на будущее, потому что всегда всё с
          неимоверной скоростью меняется. Думаю, цели сами потихоньку
          образуются, когда ты живешь и занимаешься любимым делом. Так что я
          хочу просто жить здесь с женой, работать в своей таккерии и гулять.
        </article>

        <PostFooter tags={tags} />

        <hr className="blog-post__delimiter" />
      </section>
    );
  }
}

export default Post;
