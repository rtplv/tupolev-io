import React, { Component } from 'react';
import Link from 'next/link';
import PropTypes from 'prop-types';

import './PostFooter.scss';

class PostFooter extends Component {
  static propTypes = {
    tags: PropTypes.array.isRequired,
  };

  render() {
    const { tags } = this.props;
    return (
      <article className="blog-post__footer">
        <div className="footer-tags">
          <span className="tags-title">Теги:</span>
          {tags.map((tag, index) => (
            <Link href={{ pathname: '/tag', query: { name: tag } }}>
              <a className="tags-item">
                {tag}
                {index !== tags.length - 1 && (
                  <span className="tags-item__delimiter">{', '}</span>
                )}
              </a>
            </Link>
          ))}
        </div>
      </article>
    );
  }
}

export default PostFooter;
