import React, { Component } from 'react';
import Link from 'next/link';
import PropTypes from 'prop-types';

import './PostHeader.scss';

class PostHeader extends Component {
  static propTypes = {
    meta: PropTypes.object.isRequired,
  };

  render() {
    const { meta } = this.props;
    return (
      <article className="blog-post__header">
        <div className="header-meta">
          <span className="meta-tag">
            <Link href={{ pathname: '/tag', query: { name: meta.tag } }}>
              <a>{meta.tag}</a>
            </Link>
          </span>
          <span className="meta-date">{meta.dateCreated}</span>
        </div>
        <div className="header-title">{meta.title}</div>
      </article>
    );
  }
}

export default PostHeader;
