module.exports = isDev => {
  const withSass = require('@zeit/next-sass');
  const withCSS = require('@zeit/next-css');

  return withSass(
    withCSS({
      webpack: config => {
        config.module.rules.push({
          test: /\.(png|woff|woff2|eot|ttf|svg)$/,
          loader: 'url-loader?limit=8192',
        });

        // dev mode
        if (isDev) {
          config.module.rules.push({
            enforce: 'pre',
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            loader: 'eslint-loader',
          });
        }

        return config;
      },
    }),
  );
};
