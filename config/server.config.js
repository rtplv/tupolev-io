module.exports = {
  ORIGIN_URL: 'http://localhost:3000',
  HTTP_PORT: 3000,
  HTTPS_PORT: 8443,
};
