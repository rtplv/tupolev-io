import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './bootstrap';
import '~/styles/index.scss';
import './adminLayout.scss';

import LayoutHead from './partial/Head';
import Header from './partial/Admin/Header';
// import Footer from './partial/Public/Footer';

class AdminLayout extends Component {
  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node,
    ]),
    title: PropTypes.string,
  };

  render() {
    const { children, title } = this.props;

    return (
      <div className="admin-layout">
        <LayoutHead title={title} />

        <Header />

        <main className="admin-layout__page-container">{children}</main>

        {/* <Footer /> */}
      </div>
    );
  }
}

export default AdminLayout;
