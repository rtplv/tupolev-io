import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './authLayout.scss';
import '~/styles/index.scss';

import LayoutHead from './partial/Head';

class AuthLayout extends Component {
  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node,
    ]),
    title: PropTypes.string,
  };

  render() {
    const { children, title } = this.props;

    return (
      <main className="auth-layout">
        <LayoutHead title={title} />

        {children}
      </main>
    );
  }
}

export default AuthLayout;
