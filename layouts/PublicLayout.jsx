import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './bootstrap';
import '~/styles/index.scss';
import './publicLayout.scss';

import LayoutHead from './partial/Head';
import Header from './partial/Public/Header';
// import Footer from './partial/Public/Footer';
import Plane from './partial/Public/Plane';

class PublicLayout extends Component {
  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node,
    ]),
    title: PropTypes.string,
  };

  render() {
    const { children, title } = this.props;

    return (
      <div className="public-layout">
        <LayoutHead title={title} />

        <Header />

        <main className="public-layout__page-container">{children}</main>

        {/* <Footer /> */}

        <Plane />
      </div>
    );
  }
}

export default PublicLayout;
