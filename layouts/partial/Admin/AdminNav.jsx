import React from 'react';
import cn from 'classnames';

import './AdminNav.scss';

const AdminNav = props => {
  return (
    <ul
      className={cn(
        'pinned-list',
        'header-sub-list',
        props.vertical && 'header-list--vertical',
      )}
    >
      <li>
        <a href="/admin/new-post">Написать пост</a>
      </li>
      <li>
        <a href="/admin/settings">Настройки</a>
      </li>
    </ul>
  );
};

export default AdminNav;
