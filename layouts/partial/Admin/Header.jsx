import React, { Component } from 'react';
import cn from 'classnames';
import Link from 'next/link';
import { CSSTransition } from 'react-transition-group';
import { getCSSTransitionsClassNames } from '~/utils/animations';

import { Icon } from 'semantic-ui-react';

import HeaderNav from './HeaderNav';
import SubHeader from './SubHeader';
import MobileSidebar from './MobileSidebar';

import './Header.scss';

class AdminHeader extends Component {
  state = {
    sidebarShown: false,
  };

  render() {
    const { sidebarShown } = this.state;
    return (
      <header className="admin-layout__header">
        <div className="header-main">
          <div className="header-nav-container header-container">
            <HeaderNav />
          </div>

          <div className="header-burger-container header-container">
            <div className="header-burger">
              <a href="#" onClick={this.toggleSidebar}>
                <Icon
                  className={cn(
                    sidebarShown && 'header-burger__icon--active',
                    'header-burger__icon',
                  )}
                  name="bars"
                />
              </a>
            </div>
          </div>

          <div className="header-logo-container header-container">
            <img
              src="/static/images/plane/default.svg"
              className="header-logo__icon"
              alt="лого"
            />
            <span className="header-logo__text">Туполев</span>
            <sub className="header-logo__sub">Админка</sub>
          </div>

          <div className="header-links-container header-container">
            <div className="header__signin">
              <ul className="signin-list header-list">
                <li className="social">
                  <Link href="/logout">
                    <a>Выйти</a>
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </div>

        <SubHeader />

        <CSSTransition
          in={sidebarShown}
          classNames={getCSSTransitionsClassNames('header-mobile-sidebar')}
          timeout={450}
          mountOnEnter
          unmountOnExit
        >
          <MobileSidebar />
        </CSSTransition>

        <CSSTransition
          in={sidebarShown}
          classNames={getCSSTransitionsClassNames('sidebar-overlay')}
          timeout={450}
          mountOnEnter
          unmountOnExit
        >
          <div
            className="header-mobile-sidebar-overlay"
            onClick={this.toggleSidebar}
          />
        </CSSTransition>
      </header>
    );
  }

  toggleSidebar = async event => {
    const { sidebarShown } = this.state;
    event.preventDefault();

    await this.setState({
      ...this.state,
      sidebarShown: !sidebarShown,
    });

    document.body.style.overflowY = this.state.sidebarShown ? 'hidden' : '';
  };
}

export default AdminHeader;
