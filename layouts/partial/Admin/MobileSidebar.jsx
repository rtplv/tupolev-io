import React, { Component } from 'react';

import HeaderNav from './HeaderNav';
import PinnedLinksList from './AdminNav';

import './MobileSidebar.scss';

class MobileSidebar extends Component {
  root = React.createRef();
  render() {
    return (
      <>
        <aside className="header-mobile-sidebar" ref={this.root}>
          <div className="mobile-sidebar__nav">
            <HeaderNav vertical />
          </div>
          <div className="mobile-sidebar__pinned">
            <PinnedLinksList vertical />
          </div>
        </aside>
      </>
    );
  }
}

export default MobileSidebar;
