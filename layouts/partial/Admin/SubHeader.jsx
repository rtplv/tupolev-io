import React, { Component } from 'react';
// import Link from 'next/link';
// import { Icon } from 'semantic-ui-react';

import AdminNav from './AdminNav';

import './SubHeader.scss';

class AdminSubHeader extends Component {
  render() {
    return (
      <div className="header-sub">
        <div className="header-sub__pinned-links">
          <AdminNav />
        </div>
      </div>
    );
  }
}

export default AdminSubHeader;
