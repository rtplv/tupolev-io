import React, { Component } from 'react';
import Link from 'next/link';

class PublicFooter extends Component {
  render() {
    return (
      <footer className="public-layout__footer">
        <span />
      </footer>
    );
  }
}

export default PublicFooter;
