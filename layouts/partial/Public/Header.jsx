import React, { Component } from 'react';
import cn from 'classnames';
import { CSSTransition } from 'react-transition-group';
import { getCSSTransitionsClassNames } from '~/utils/animations';

import { Icon } from 'semantic-ui-react';

import SubHeader from './SubHeader';
import HeaderNav from './HeaderNav';
import MobileSidebar from './MobileSidebar';

import './Header.scss';

class PublicHeader extends Component {
  state = {
    sidebarShown: false,
  };

  render() {
    const { sidebarShown } = this.state;
    return (
      <header className="public-layout__header">
        <div className="header-main">
          <div className="header-nav-container header-container">
            <HeaderNav />
          </div>

          <div className="header-burger-container header-container">
            <div className="header-burger">
              <a href="#" onClick={this.toggleSidebar}>
                <Icon
                  className={cn(
                    sidebarShown && 'header-burger__icon--active',
                    'header-burger__icon',
                  )}
                  name="bars"
                />
              </a>
            </div>
          </div>

          <div className="header-logo-container header-container">
            <img
              src="/static/images/plane/default.svg"
              className="header-logo__icon"
              alt="лого"
            />
            <span className="header-logo__text">Туполев</span>
          </div>

          <div className="header-links-container header-container">
            <div className="header__socials">
              <ul className="socials-list header-list">
                <li className="social-vk">
                  <a
                    href="https://vk.com/rtplv"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <Icon name="vk" />
                  </a>
                </li>
                <li className="social-telegram">
                  <a
                    href="tg://resolve?domain=rtplv"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <Icon name="telegram" />
                  </a>
                </li>
                <li className="social-github">
                  <a
                    href="https://github.com/rtplv"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    <Icon name="github" />
                  </a>
                </li>
              </ul>
            </div>
            {/* <div className="header__signin">
              <ul className="signin-list header-list">
                <li className="social">
                  <Link href="/login">
                    <a>Войти</a>
                  </Link>
                </li>
              </ul>
            </div> */}
          </div>
        </div>

        <SubHeader />

        <CSSTransition
          in={sidebarShown}
          classNames={getCSSTransitionsClassNames('header-mobile-sidebar')}
          timeout={450}
          mountOnEnter
          unmountOnExit
        >
          <MobileSidebar />
        </CSSTransition>

        <CSSTransition
          in={sidebarShown}
          classNames={getCSSTransitionsClassNames('sidebar-overlay')}
          timeout={450}
          mountOnEnter
          unmountOnExit
        >
          <div
            className="header-mobile-sidebar-overlay"
            onClick={this.toggleSidebar}
          />
        </CSSTransition>
      </header>
    );
  }

  toggleSidebar = async event => {
    const { sidebarShown } = this.state;
    event.preventDefault();

    await this.setState({
      ...this.state,
      sidebarShown: !sidebarShown,
    });

    document.body.style.overflowY = this.state.sidebarShown ? 'hidden' : '';
  };
}

export default PublicHeader;
