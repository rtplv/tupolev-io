import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import Link from 'next/link';

import './HeaderNav.scss';

const HeaderNav = props => {
  return (
    <nav>
      <ul
        className={cn(
          'header-nav',
          'header-list',
          props.vertical && 'header-list--vertical',
        )}
      >
        <li>
          <Link href="/">
            <a>Главная</a>
          </Link>
        </li>
        <li>
          <Link href="/projects">
            <a>Проекты</a>
          </Link>
        </li>
        <li>
          <Link href="/about">
            <a>Обо мне</a>
          </Link>
        </li>
      </ul>
    </nav>
  );
};

HeaderNav.propTypes = {
  vertical: PropTypes.bool,
};

export default HeaderNav;
