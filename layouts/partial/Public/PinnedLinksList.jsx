import React from 'react';
import cn from 'classnames';

import './PinnedLinksList.scss';

const PinnedLinksList = props => {
  return (
    <ul
      className={cn(
        'pinned-list',
        'header-sub-list',
        props.vertical && 'header-list--vertical',
      )}
    >
      <li>
        <a
          href="https://react.tupolev.io"
          target="_blank"
          rel="noopener noreferrer"
        >
          <span role="img" aria-label="Флаг">
            🇷🇺
          </span>{' '}
          Документация React на русском
        </a>
      </li>
    </ul>
  );
};

export default PinnedLinksList;
