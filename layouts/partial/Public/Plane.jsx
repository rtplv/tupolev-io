import React, { Component } from 'react';
import cn from 'classnames';

import './Plane.scss';

class Plane extends Component {
  state = {
    isShowing: false,
  };

  componentDidMount() {
    window.addEventListener('scroll', this.onScrollHandler);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.onScrollHandler);
  }

  render() {
    const { isShowing } = this.state;
    return (
      <div
        className={cn('plane-wrapper', !isShowing && 'plane-wrapper--hidden')}
      >
        <a href="#" onClick={this.scrollToTop}>
          <svg
            version="1.1"
            className="plane"
            id="plane"
            xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink"
            x="0px"
            y="0px"
            viewBox="0 0 512.001 512.001"
            xmlSpace="preserve"
          >
            <g>
              <g>
                <path
                  d="M507.137,411.187L287.305,31.653c-6.438-11.116-18.434-18.021-31.304-18.021c-12.87,0-24.866,6.905-31.304,18.021
			L4.864,411.187c-7.298,12.601-6.318,28.235,2.499,39.831c6.795,8.938,17.564,14.272,28.805,14.272h0.003
			c3.005-0.001,6.009-0.377,8.929-1.119l150.892-38.377l33.447,57.369c5.466,9.38,15.644,15.206,26.561,15.206
			c10.916,0,21.094-5.826,26.561-15.204l33.43-57.342l150.917,38.35c2.917,0.741,5.92,1.116,8.924,1.116
			c11.246,0,22.014-5.336,28.807-14.275C513.454,439.42,514.435,423.786,507.137,411.187z M248.165,480.509
			c-2.113-1.275-3.914-3.056-5.184-5.237l-31.188-53.495l36.373-9.25V480.509z M269.022,475.273
			c-1.271,2.181-3.072,3.961-5.184,5.236v-67.937l36.354,9.238L269.022,475.273z M492.162,441.531
			c-3.848,5.063-9.952,8.086-16.33,8.086c-1.704,0-3.409-0.213-5.065-0.634l-206.93-52.582V48.377c0-4.329-3.508-7.836-7.836-7.836
			c-4.329,0-7.836,3.508-7.836,7.836v347.979L41.241,448.982c-1.66,0.421-3.367,0.635-5.075,0.636
			c-6.376-0.001-12.478-3.024-16.328-8.085c-4.98-6.549-5.533-15.377-1.413-22.491L238.259,39.507
			c3.644-6.294,10.443-10.204,17.741-10.204c7.298,0,14.097,3.91,17.741,10.204l219.831,379.535
			C497.694,426.156,497.141,434.982,492.162,441.531z"
                />
              </g>
            </g>
            <g />
            <g />
            <g />
            <g />
            <g />
            <g />
            <g />
            <g />
            <g />
            <g />
            <g />
            <g />
            <g />
            <g />
            <g />
          </svg>
        </a>
      </div>
    );
  }

  onScrollHandler = () => {
    const { isShowing } = this.state;

    if (window.scrollY > 101 && !isShowing) {
      this.setState({
        ...this.state,
        isShowing: true,
      });
    } else if (window.scrollY < 101 && isShowing) {
      this.setState({
        ...this.state,
        isShowing: false,
      });
    }
  };

  scrollToTop = event => {
    event.preventDefault();

    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  };
}

export default Plane;
