import React, { Component } from 'react';
// import Link from 'next/link';
// import { Icon } from 'semantic-ui-react';

import PinnedLinksList from './PinnedLinksList';

import './SubHeader.scss';

class PublicSubHeader extends Component {
  render() {
    return (
      <div className="header-sub">
        <div className="header-sub__pinned-links">
          <PinnedLinksList />
        </div>
      </div>
    );
  }
}

export default PublicSubHeader;
