const isDev = process.env.NODE_ENV !== 'production';

module.exports = require('./config/next.config')(isDev);
