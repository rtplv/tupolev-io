import React, { Component } from 'react';
import Router from 'next/router';

class AdminPage extends Component {
  static async getInitialProps({ res }) {
    if (res) {
      res.writeHead(302, {
        Location: '/admin/new-post',
      });
      res.end();
    } else {
      Router.push('/admin/new-post');
    }
    return {};
  }
}

export default AdminPage;
