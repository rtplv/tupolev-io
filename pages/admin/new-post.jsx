import React, { Component } from 'react';

import AdminLayout from '~/layouts/AdminLayout';

class NewPostPage extends Component {
  render() {
    return (
      <AdminLayout title="Написать пост">
        <div className="admin-new-post-page">Форма добавления поста</div>
      </AdminLayout>
    );
  }
}

export default NewPostPage;
