import React, { Component } from 'react';

import AdminLayout from '~/layouts/AdminLayout';

class SettingsPage extends Component {
  render() {
    return (
      <AdminLayout title="Написать пост">
        <div className="admin-settings-page">Форма добавления поста</div>
      </AdminLayout>
    );
  }
}

export default SettingsPage;
