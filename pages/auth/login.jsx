import React, { Component } from 'react';

import AuthLayout from '~/layouts/AuthLayout';

class LoginPage extends Component {
  render() {
    return (
      <AuthLayout title="Вход">
        <div style={{ width: '320px', height: '404px' }}>
          <span>Логин</span>
        </div>
      </AuthLayout>
    );
  }
}

export default LoginPage;
