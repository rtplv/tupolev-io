import React, { Component } from 'react';

import PublicLayout from '~/layouts/PublicLayout';

import PostsContainer from '~/components/index/PostsContainer';

class IndexPage extends Component {
  render() {
    return (
      <PublicLayout title="Главная">
        <div className="index-page">
          <PostsContainer />
        </div>
      </PublicLayout>
    );
  }
}

export default IndexPage;
