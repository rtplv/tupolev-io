import React, { Component } from 'react';
import PropTypes from 'prop-types';

import response from '~/utils/response';

import PublicLayout from '~/layouts/PublicLayout';
import Post from '~/components/post/Post';

class PostPage extends Component {
  static async getInitialProps({ query, res }) {
    if (!query.id && res) {
      response.notFound();
    }
    return { query: query.id };
  }
  static propTypes = {
    query: PropTypes.string,
  };

  render() {
    return (
      <PublicLayout title="Название поста">
        <div className="post-page">
          {/* Страница поста
          {this.props.query} */}
          <Post />
        </div>
      </PublicLayout>
    );
  }
}

export default PostPage;
