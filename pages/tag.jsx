import React, { Component } from 'react';
import PropTypes from 'prop-types';

import PublicLayout from '~/layouts/PublicLayout';

import response from '~/utils/response';

class TagPage extends Component {
  static async getInitialProps({ query, res }) {
    if (!query.name && res) {
      response.notFound();
    }
    return { query: query.name };
  }
  static propTypes = {
    query: PropTypes.string,
  };

  render() {
    return (
      <PublicLayout title="Название тэга">
        <div className="tag-page">
          Страница тэг
          {this.props.query}
        </div>
      </PublicLayout>
    );
  }
}

export default TagPage;
