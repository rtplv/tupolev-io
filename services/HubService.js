import api from './api';

export default {
  fetchLinks() {
    return api().get('hub/links');
  },
  fetchLinkById(id) {
    return api().get(`hub/links/${id}`);
  },
  createLink(params) {
    return api().post(`hub/link/create`, params);
  },
  updateLink(params) {
    return api().put(`hub/link/update`, params);
  },
  deleteLink(data) {
    return api().delete(`hub/link/delete`, { data });
  },
};
