import axios from 'axios';

const { ORIGIN_URL } = require('~/config/server.config');

export default () => {
  const params = {
    baseURL: `${ORIGIN_URL}/api/`,
  };

  const instance = axios.create(params);

  return instance;
};
