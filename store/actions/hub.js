import { EXAMPLE_CONST } from '../actionTypes/hub';

export const setExampleStr = newStr => ({
  type: EXAMPLE_CONST,
  payload: newStr,
});
