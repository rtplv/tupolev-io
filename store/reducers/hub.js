import { EXAMPLE_CONST } from '../actionTypes/hub';

const initialState = {
  exampleStr: '',
};

export default (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case EXAMPLE_CONST:
      return {
        ...state,
        exampleStr: payload,
      };
    default:
      return state;
  }
};
