import hubReducer from './reducers/hub';
import { combineReducers } from 'redux';

export default combineReducers({
  hub: hubReducer,
});
