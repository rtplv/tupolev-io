/**
 * Generate class objects for CSSTransitions lib
 * @param {String} target
 */
export const getCSSTransitionsClassNames = target => ({
  enter: `${target}--enter`,
  enterActive: `${target}--enter-active`,
  exit: `${target}--exit`,
  exitActive: `${target}--exit-active`,
});
