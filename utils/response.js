const notFound = () => {
  const err = new Error();
  err.code = 'ENOENT';
  throw err;
};

export default {
  notFound,
};
